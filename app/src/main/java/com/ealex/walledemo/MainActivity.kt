package com.ealex.walledemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.meituan.android.walle.WalleChannelReader
import com.tencent.bugly.crashreport.CrashReport
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var channelInfo = WalleChannelReader.getChannelInfo(this.getApplicationContext());
        if (channelInfo != null) {
            var channel = channelInfo.getChannel();
            var extraInfo = channelInfo.getExtraInfo();
            mChannelInfoTv.text = channel
//            CrashReport.setAppChannel(this@MainActivity, channel)
        } else {
            mChannelInfoTv.text = "ealex"
//            CrashReport.setAppChannel(this@MainActivity, "ealex")
        }
        // 或者也可以直接根据key获取
        var value = WalleChannelReader.get(this@MainActivity, "buildtime");
        mChannelInfoTv.setOnClickListener {
            CrashReport.testJavaCrash()
        }
    }
}
