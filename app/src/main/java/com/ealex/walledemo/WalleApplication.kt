package com.ealex.walledemo

import android.app.Application
import com.meituan.android.walle.WalleChannelReader
import com.tencent.bugly.crashreport.CrashReport
import kotlinx.android.synthetic.main.activity_main.*

class WalleApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        CrashReport.initCrashReport(getApplicationContext(), "4081608239", false);
        var channelInfo = WalleChannelReader.getChannelInfo(this.getApplicationContext())
        if (channelInfo != null) {
            var channel = channelInfo.getChannel()
            var extraInfo = channelInfo.getExtraInfo()
            CrashReport.setAppChannel(this@WalleApplication, channel)
        }else{
            CrashReport.setAppChannel(this@WalleApplication, "ealex")
        }
    }
}